#! /usr/bin/ruby

require 'fileutils'

`./clean_test.rb`

if File.exist?("source/correction.cmd") &&
    File.exist?("source/run.sh")

    f = File.open("source/correction.cmd", "r")
    i = 0
    while 42
        content = ""
        while (line = f.gets) && line.chomp != "\\cut" && !content.match(/###COMPILE/) && !content.match(/###CONFIG/)
            if (line.chomp != "")
                content += line
            end
        end
        if content.match(/###COMPILE/)
            FileUtils.mkdir("COMPILE")
            while (line = f.gets) && line.chomp != "\\cut"
                if (line.chomp != "")
                    content += line
                end
            end
            FileUtils.cp("source/Makefile", "COMPILE/")
            fout = File.open("COMPILE/compile.cmd", "w+")
            fout.write(content)
            fout.close()
        elsif content.match(/###CONFIG/)
            FileUtils.mkdir("CONFIG")
            while (line = f.gets) && line.chomp != "\\cut"
                if (line.chomp != "")
                    content += line
                end
            end
            fout = File.open("CONFIG/config.cmd", "w+")
            fout.write(content)
            fout.close()
        elsif content != ""
            FileUtils.mkdir(i.to_s())
            if File.exist?("source/Makefile")
                FileUtils.cp("source/Makefile", "#{i}/")
            end
            FileUtils.cp("source/run.sh", "#{i}/")
            fout = File.open("#{i}/test.cmd", "a")
            fout.write(content)
            fout.close()
            i += 1
        else
            f.close()
            break
        end
    end
end
