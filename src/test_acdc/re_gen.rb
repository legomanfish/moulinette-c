#! /usr/bin/ruby

require 'fileutils'
require 'open3'

require '../core/moulinetteCore'

puts "#########################################################".magenta
puts "#########################################################".magenta
puts "###################### reMOULINETTE #####################".magenta
puts "#########################################################".magenta
puts "#########################################################\n\n".magenta

if File.exist?("acdc") && File.directory?("acdc")
    `rm -r acdc`
end
if File.exist?("DIRNOTE")
    `rm DIRNOTE`
end

config_file = init_config_file("../test_acdc/CONFIG/config.cmd")
gen_repo(".", "..", config_file)

puts "\nFinish! Now, you can test the source code with ./run_test.rb".cyan
