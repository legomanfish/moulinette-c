#! /usr/bin/ruby

require './core/moulinetteCore'

puts "#########################################################".magenta
puts "#########################################################".magenta
puts "###################### MOULINETTE #######################".magenta
puts "#########################################################".magenta
puts "#########################################################\n\n".magenta

`cd test_acdc && ./create_test.rb`

clean_rootDir(".")
file_printFileRepo(".")

nbCompile = 0
nbTest = 0
nbRepoCompile = 0
nbRepo = 0

config_file = init_config_file("test_acdc/CONFIG/config.cmd")

for x in Dir["./*"]
    if File.extname(x) == ".zip"
        puts "\n#########################################################".blue
        puts x.cyan
        nbRepo += 1

        $find_authors = false;

        dirname = File.basename(x, ".zip")
        if File.exist?(File.basename(dirname)) && File.directory?(File.basename(dirname))
            FileUtils.rm_rf("#{dirname}/acdc")
            puts "Dir already exist! Remove #{dirname}/acdc...".cyan
        else
            `unzip #{x} -d #{dirname}`
        end

        statOut = gen_repo(dirname, ".", config_file)
        if statOut.nbTest >= 0 && statOut.nbCompile >= 0
            nbTest += statOut.nbTest
            nbCompile += statOut.nbCompile
            nbRepoCompile += 1
        end

        puts "##########################################################\n".blue
    end
end

file_printFileRepo(".")

if nbRepo > 0
    pourcRepoCompile = (nbRepoCompile * 100) / nbRepo
    puts "#{pourcRepoCompile}% of repo compile (#{nbRepoCompile} from #{nbRepo})".magenta
    puts "#{$nbFailAuthors} fails AUTHORS".magenta
    if nbTest > 0
        pourc = (nbCompile * 100) / nbTest
        puts "#{pourc}% of gen compile (#{nbCompile} from #{nbTest})".magenta
    end
end

puts "Don't forget to check the code and test no-testing part!".cyan
