#! /usr/bin/ruby

require 'fileutils'

require_relative 'moulinetteStruct'

def init_config_file(fname)
    config_file = ConfigFile.new
    config_file.timeout = 5
    config_file.extension = Array.new
    config_file.mainPrototype = Array.new
    config_file.mainReplacePrototype = ""

    if File.exist?(fname)
        f = File.open(fname, "r")
        while line = f.gets
            if line.chomp == "\\timeout"
                while (line = f.gets) && line.chomp != "\\end"
                    config_file.timeout = line.to_i
                end
            elsif line.chomp == "\\extension"
                while (line = f.gets) && line.chomp != "\\end"
                    config_file.extension.push(line.chomp)
                end
            elsif line.chomp == "\\mainPrototype"
                while (line = f.gets) && line.chomp != "\\end"
                    config_file.mainPrototype.push(line.chomp)
                end
            elsif line.chomp == "\\mainReplacePrototype"
                while (line = f.gets) && line.chomp != "\\end"
                    config_file.mainReplacePrototype = line.chomp
                end
            end
        end
        f.close()
    else
        config_file.extension.push(".cs")
    end
    return config_file
end

def init_cmd_file(fname, config_file)
    cmd_file = CmdFile.new
    cmd_file.main = ""
    cmd_file.fun = ""
    cmd_file.stdin = Array.new
    cmd_file.stdout = ""
    cmd_file.stderr = ""
    cmd_file.dirsource = Array.new
    cmd_file.justprint = false
    cmd_file.forbidden = Array.new
    cmd_file.shfile = ""
    cmd_file.num = ""
    cmd_file.timeout = config_file.timeout
    cmd_file.l_edit = Array.new

    f = File.open(fname, "r")
    while line = f.gets
        if line.chomp == "\\main"
            while (line = f.gets) && line.chomp != "\\end"
                cmd_file.main += line
            end
        elsif line.chomp == "\\num"
            while (line = f.gets) && line.chomp != "\\end"
                cmd_file.num += line.chomp
            end
        elsif line.chomp == "\\fun"
            while (line = f.gets) && line.chomp != "\\end"
                cmd_file.fun += line
            end
        elsif line.chomp == "\\stdin"
            while (line = f.gets) && line.chomp != "\\end"
                cmd_file.stdin.push(line)
            end
        elsif line.chomp == "\\stdout"
            while (line = f.gets) && line.chomp != "\\end"
                cmd_file.stdout += line
            end
        elsif line.chomp == "\\stderr"
            while (line = f.gets) && line.chomp != "\\end"
                cmd_file.stderr += line
            end
        elsif line.chomp == "\\dirsource"
            while (line = f.gets) && line.chomp != "\\end"
                cmd_file.dirsource.push(line.chomp)
            end
        elsif line.chomp == "\\shfile"
            while (line = f.gets) && line.chomp != "\\end"
                cmd_file.shfile += line
            end
        elsif line.chomp == "\\justprint"
            cmd_file.justprint = true
        elsif line.chomp == "\\forbidden"
            while (line = f.gets) && line.chomp != "\\end"
                cmd_file.forbidden.push(line.chomp)
            end
        elsif line.chomp == "\\timeout"
            while (line = f.gets) && line.chomp != "\\end"
                cmd_file.timeout = line.to_i
            end
        elsif line.chomp == "\\edit"
            edit = EditStruct.new
            edit.prototype = ""
            edit.replace = ""
            edit.custom = ""

            while (line = f.gets) && line.chomp != "\\end"
                if line.chomp == "#\\prototype"
                    while (line = f.gets) && line.chomp != "\\end" && line.chomp != "#\\end"
                        edit.prototype = line.chomp
                    end
                elsif line.chomp == "#\\replace"
                    while (line = f.gets) && line.chomp != "\\end" && line.chomp != "#\\end"
                        edit.replace = line.chomp
                    end
                end
            end

            if edit.prototype != "" && edit.replace != ""
                cmd_file.l_edit.push(edit)
            end
        elsif line.chomp == "\\customfun"
            edit = EditStruct.new
            edit.prototype = ""
            edit.replace = ""
            edit.custom = ""

            while (line = f.gets) && line.chomp != "\\end"
                if line.chomp == "#\\prototype"
                    while (line = f.gets) && line.chomp != "\\end" && line.chomp != "#\\end"
                        edit.prototype = line.chomp
                    end
                elsif line.chomp == "#\\replace"
                    while (line = f.gets) && line.chomp != "\\end" && line.chomp != "#\\end"
                        edit.replace = line.chomp
                    end
                elsif line.chomp == "#\\custom"
                    while (line = f.gets) && line.chomp != "\\end" && line.chomp != "#\\end"
                        edit.custom += line
                    end
                end
            end

            if edit.prototype != "" && edit.replace != "" && edit.custom != ""
                cmd_file.l_edit.push(edit)
            end
        end
    end
    f.close()
    return cmd_file
end
