#! /usr/bin/ruby

require 'timeout'
require 'open3'

require_relative 'moulinetteStruct'

def run_test(cmd_file, number, directory)
    Open3.popen3("cd #{directory}/acdc/#{number} && ./run.sh") {|i, o, e, t|
        pid = t.pid
        puts "#----------------#".cyan
        puts cmd_file.main.cyan
        input = ""
        for input in cmd_file.stdin
            i.puts(input)
        end 

        begin
            Timeout.timeout(cmd_file.timeout) do
                stdout = ""
                while line = o.gets do
                    stdout += line
                end
                stderr = ""
                while line = e.gets do
                    stderr += line
                end
                if !cmd_file.justprint
                    if stdout.match(/\A#{cmd_file.stdout.chomp}\Z/) && stderr.match(/\A#{cmd_file.stderr.chomp}\Z/)
                        puts ("[TEST #{number}][" + cmd_file.num + "] OK").green
                        puts ("stdout (Student):\n" + stdout).green
                        puts ("stderr (Student):\n" + stderr).green
                    else
                        puts ("[TEST #{number}][" + cmd_file.num + "] FAIL").red
                        if !stdout.match(/\A#{cmd_file.stdout.chomp}\Z/)
                            puts ("stdout (Expected):\n" + cmd_file.stdout).red
                            puts ("stdout (Student):\n" + stdout).red
                        end
                        if !stderr.match(/\A#{cmd_file.stderr.chomp}\Z/)
                            puts ("stderr (Expected):\n" + cmd_file.stderr).red
                            puts ("stderr (Student):\n" + stderr).red
                        end
                    end
                else
                    puts ("[TEST #{number}][" + cmd_file.num + "] Just Print Result").blue
                    puts ("stdout:\n" + stdout).blue
                    puts ("stderr:\n" + stderr).blue
                end
            end
        rescue Timeout::Error
            puts ("[TEST #{number}][" + cmd_file.num + "] TIMEOUT").red
            Process.kill('TERM', pid)
            `pkill mono`
        end
    }
end

def run_testSuit(directory)
    if File.exist?("#{directory}/acdc") && File.directory?("#{directory}/acdc")
        config_file = init_config_file("../test_acdc/CONFIG/config.cmd")
        x = 0;
        while File.exist?("acdc/#{x}")
            cmd_file = init_cmd_file("acdc/#{x}/test.cmd", config_file)
            run_test(cmd_file, x, directory)
            x += 1
        end
        puts "#----------------#".cyan
        puts "\nFinish! Don't forget to look the code and test the no-testing code!".magenta
    else
        puts "[ERROR] #{directory}/acdc folder doesn't found!".red
    end
end
