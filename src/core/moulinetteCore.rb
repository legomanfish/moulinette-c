#! /usr/bin/ruby

require 'fileutils'

require_relative 'moulinetteStruct'
require_relative 'moulinetteInit'
require_relative 'moulinetteCompile'
require_relative 'moulinetteAuthors'
require_relative 'moulinetteMakefile'
require_relative 'moulinetteFile'
require_relative 'moulinetteRun'
require_relative 'moulinetteGen'
require_relative 'moulinetteClean'
