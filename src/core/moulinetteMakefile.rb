#! /usr/bin/ruby

require 'fileutils'

require_relative 'moulinetteStruct'

def makefile_edit(fname, source_str)
    content = File.read(fname)
    content["SRC="] = "SRC=" + source_str
    f = File.open(fname, "w+")
    f.write(content)
    f.close()
end
