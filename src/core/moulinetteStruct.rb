#! /usr/bin/ruby

class String
    def red;            "\033[31m#{self}\033[0m" end
    def green;          "\033[32m#{self}\033[0m" end
    def blue;           "\033[34m#{self}\033[0m" end
    def magenta;        "\033[35m#{self}\033[0m" end
    def cyan;           "\033[36m#{self}\033[0m" end
end

EditStruct = Struct.new(:prototype, :replace, :custom)
ConfigFile = Struct.new(:timeout, :extension, :mainPrototype, :mainReplacePrototype)
CmdFile = Struct.new(:main, :num, :fun, :stdin, :stdout, :stderr, :dirsource, :justprint, :forbidden, :shfile, :timeout, :l_edit)
StatCompile = Struct.new(:nbTest, :nbCompile)

$nbFailAuthors = 0
$find_authors = false;
