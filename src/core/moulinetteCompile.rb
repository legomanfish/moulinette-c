#! /usr/bin/ruby

require 'fileutils'

require_relative 'moulinetteStruct'

def compile_check(dirname, source, pathRootDir)
    if !File.exist?("#{pathRootDir}/test_acdc/COMPILE") || !File.exist?("#{pathRootDir}/test_acdc/COMPILE/compile.cmd")
        return 1
    end
    f = File.open("#{pathRootDir}/test_acdc/COMPILE/compile.cmd")
    i = 0
    while line = f.gets
        if line.match(/\\dirsource/)
            dirSource = Array.new
            while (line = f.gets) && line.chomp != "\\end"
                dirSource.push(line.chomp)
            end
            x = 0
            while x < dirSource.size
                src = Array.new
                for elt in source
                    if dirSource[x] == File.basename(File.dirname(elt))
                        src.push(elt)
                    end
                end
                if src.size > 0
                    FileUtils.mkdir("#{dirname}/tmp")
                    srcFile = ""
                    for elt in src
                        puts "[COMPILE][#{i}][CP] #{elt} to #{dirname}/tmp/".green
                        FileUtils.cp(elt, "#{dirname}/tmp/")
                        srcFile += File.basename(elt) + " "
                    end
                    FileUtils.cp("#{pathRootDir}/test_acdc/COMPILE/Makefile", "#{dirname}/tmp/")
                    makefile_edit("#{dirname}/tmp/Makefile", srcFile)


                    out = ""
                    Open3.popen3("cd #{dirname}/tmp && make") {|p, o, e, t|
                        stdout = ""
                        while line = o.gets
                            stdout += line
                        end
                        stderr = ""
                        while line = e.gets
                            stderr += line
                        end
                        if stderr == "" && stdout == ""
                            puts "[COMPILE][#{i}][TEST] OK".green
                        else
                            puts "[COMPIL][#{i}][TEST] FAIL".red
                            puts ("stdout:\n" + stdout).red
                            puts ("stderr:\n" + stderr).red
                            out = "error"

                            f = File.open("#{dirname}/COMPILE", "w+")
                            f.write("[COMPIL][#{i}][TEST] FAIL\n" + "stdout:\n" + stdout + "\nstderr:\n" + stderr + "\n")
                            f.close()
                        end
                    }

                    `rm -r #{dirname}/tmp`
                    if out != ""
                        return 0
                    end
                    break
                end
                x += 1
            end
            if x > 0 && x >= dirSource.size
                puts "[COMPILE][WARNING] #{dirSource[0]} not found!".red
            end
            i += 1
        end
    end
    f = File.open("#{dirname}/COMPILE", "w+")
    f.write("OK")
    f.close()
    return 1
end

def compile_genCheck(path, i)
    compile = true
    Open3.popen3("cd #{path}/acdc/#{i}/ && make") {|p, o, e, t|
        stdout = ""
        while line = o.gets
            stdout += line
        end
        stderr = ""
        while line = e.gets
            stderr += line
        end
        if stderr == "" && stdout == ""
            puts "[COMPIL][TEST #{i}] OK".green
        else
            puts "[COMPIL][TEST #{i}] FAIL".red
            puts ("stdout:\n" + stdout).red
            puts ("stderr:\n" + stderr).red
            compile = false
        end
    }
    return compile
end
