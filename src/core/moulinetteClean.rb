#! /usr/bin/ruby

require_relative 'moulinetteStruct'

def clean_testConfigDir(pathRoot)
    if File.exist?("#{pathRoot}/test_acdc/COMPILE")
        `rm -r #{pathRoot}/test_acdc/COMPILE`
    end
    if File.exist?("#{pathRoot}/test_acdc/CONFIG")
        `rm -r #{pathRoot}/test_acdc/CONFIG`
    end
    i = 0
    while File.exist?("#{pathRoot}/test_acdc/#{i}")
        `rm -r #{pathRoot}/test_acdc/#{i}`
        i += 1
    end
end

def clean_rootDir(pathRoot)
    puts "#------- Clean Root Dir -------#".cyan
    folder = Dir["#{pathRoot}/*"]
    for x in folder
        if File.directory?(x) && File.exist?(x + ".zip")
            puts "Remove #{x}".cyan
            `rm -r #{x}`
        end
    end
end
