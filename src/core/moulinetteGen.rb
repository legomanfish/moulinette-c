#! /usr/bin/ruby

require_relative 'moulinetteStruct'
require_relative 'moulinetteFile'
require_relative 'moulinetteCompile'

def gen_cpFile(path, pathRoot, config_file, i)
    FileUtils.mkdir("#{path}/acdc/#{i}")
    FileUtils.cp("#{pathRoot}/test_acdc/#{i}/test.cmd", "#{path}/acdc/#{i}/")

    cmd_file = init_cmd_file("#{path}/acdc/#{i}/test.cmd", config_file)

    if (cmd_file.shfile == "")
        FileUtils.cp("#{pathRoot}/test_acdc/#{i}/run.sh", "#{path}/acdc/#{i}/")
    else
        FileUtils.cp("#{pathRoot}/test_acdc/#{cmd_file.shfile.chomp}", "#{path}/acdc/#{i}/run.sh")
    end
    return cmd_file
end

def gen_repo(path, pathRoot, config_file)
    stat = StatCompile.new
    stat.nbTest = -1
    stat.nbCompile = -1

    FileUtils.mkdir("#{path}/acdc")

    puts "\n#------- Search File -------#".cyan
    source = file_findSource(path, path, config_file)

    if (!$find_authors)
        puts "AUTHORS not found".red
        noteFile = File.open("#{path}/NOTE", "w+");
        noteFile.write("AUTHORS not found");
        noteFile.close();
        $nbFailAuthors += 1
    end

    puts "\n#------- Test Compile -------#".cyan
    compile = compile_check(path, source, pathRoot)

    puts "\n#------- Gen Test -------#".cyan
    if compile == 0
        puts "Fail compile, I can't gen test".red
    else
        stat.nbTest = 0
        stat.nbCompile = 0

        i = 0
        while File.exist?("#{pathRoot}/test_acdc/#{i}")
            if File.exist?("#{pathRoot}/test_acdc/#{i}/run.sh") && File.exist?("#{pathRoot}/test_acdc/#{i}/test.cmd")

                cmd_file = gen_cpFile(path, pathRoot, config_file, i)
                source_str = file_searchForbidden(cmd_file, source)

                for x in source
                    if cmd_file.dirsource.include?(File.basename(File.dirname(x)))
                        puts ("[CP][TEST #{i}] " + x + " to #{path}/acdc/#{i}/").green
                        content = File.read(x)
                        if cmd_file.main.chomp != "" && !config_file.mainPrototype.empty?() && config_file.mainReplacePrototype != ""
                            for mainPrototype in config_file.mainPrototype
                                if content.include?(mainPrototype)
                                    content[mainPrototype] = cmd_file.fun + "\n" + mainPrototype  + "{ " + cmd_file.main.chomp + " }\n\n" + config_file.mainReplacePrototype
                                    break
                                end
                            end
                        end
                        for edit in cmd_file.l_edit
                            if edit.custom == ""
                                content = content.gsub(/#{edit.prototype}/, edit.replace)
                            elsif content.include?(edit.prototype)
                                content[edit.prototype] = "#{edit.prototype}#{edit.custom}\n\n#{edit.replace}"
                            end
                        end
                        f = File.open(("#{path}/acdc/#{i}/" + File.basename(x)), "w+")
                        f.write(content)
                        f.close()
                    end
                end

                if File.exist?("#{pathRoot}/test_acdc/#{i}/Makefile")
                    FileUtils.cp("#{pathRoot}/test_acdc/#{i}/Makefile", "#{path}/acdc/#{i}/")
                    makefile_edit("#{path}/acdc/#{i}/Makefile", source_str)
                    compile = compile_genCheck(path, i)
                    stat.nbTest += 1
                    if compile
                        stat.nbCompile += 1
                    end
                end
            end
            i += 1
        end
    end
    puts "\n#------- CP Ruby File -------#".cyan
    `cp #{pathRoot}/test_acdc/re_gen.rb #{path}/`
    puts "[GEN] #{path}/re_gen.rb".green
    `cp #{pathRoot}/test_acdc/run_test.rb #{path}/`
    puts "[GEN] #{path}/run_test.rb".green
    return stat
end
