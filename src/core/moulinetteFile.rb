#! /usr/bin/ruby

require 'fileutils'

require_relative 'moulinetteStruct'

def file_findSource(dir, dirname, config_file)
    source = Array.new
    file = Dir[dir + "/*"]
    for n in file
        if File.basename(n, File.extname(n)).match(/AUTHORS/)
            puts ("[FILE] find " + n).green
            $find_authors = true
            authors_check(n, dirname)
        elsif File.basename(n, File.extname(n)).match(/README/)
            puts ("[FILE] find " + n).green
            readmeContent = File.read(n);
            readmeFile = File.open("#{dirname}/README", "w+");
            readmeFile.write(readmeContent);
            readmeFile.close();
        elsif File.directory?(n)
            if File.basename(n) == "bin" || File.basename(n) == "obj"
                puts "#{File.basename(n)} folder found!".red
                dirFile = File.open("#{dirname}/DIRNOTE", "a");
                dirFile.write("#{File.basename(n)} folder found!\n");
                dirFile.close();
            else
                source.concat(file_findSource(n, dirname, config_file))
            end
        elsif config_file.extension.include?(File.extname(n))
            puts ("[FILE] find " + n).green
            source.push(n)
        end
    end
    return source
end

def file_printFileRepo(path)
    if File.exist?("#{path}/NOTE")
        puts "#----- NOTE -----#".cyan
        contentNoteFile = File.read("#{path}/NOTE")
        if (contentNoteFile.chomp != "OK")
            puts contentNoteFile.red
        else
            puts contentNoteFile.cyan
        end
    end
    if File.exist?("#{path}/README")
        puts "#---- README ----#".cyan
        puts File.read("#{path}/README").cyan;
    end
    if File.exist?("#{path}/DIRNOTE")
        puts "#---- DIRNOTE ----#".cyan
        puts File.read("#{path}/DIRNOTE").red;
    end
    if File.exist?("#{path}/COMPILE")
        puts "#---- COMPILE ----#".cyan
        contentCompileFile = File.read("#{path}/COMPILE");
        if (contentCompileFile.chomp == "OK")
            puts (contentCompileFile + "\n").cyan
        else
            puts contentCompileFile.red
        end
    end
    if File.exist?("#{path}/INFO")
        puts File.read("#{path}/INFO").cyan
    end
end

def file_searchForbidden(cmd_file, source)
    source_str = ""
    for u in source
        if cmd_file.dirsource.include?(File.basename(File.dirname(u)))
            code = File.read(u)
            for forb in cmd_file.forbidden
                if code.include?(forb)
                    puts "[WARNING][FORBIDDEN] `#{forb}' is use on #{u}".red
                end
            end
            source_str += File.basename(u) + " "
        end
    end
    return source_str
end
