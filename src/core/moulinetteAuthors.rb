#! /usr/bin/ruby

require_relative 'moulinetteStruct'

def authors_checkContent(content)
    return content.match(/\A\* [a-z0-9]{1,6}\_[a-z0-9](\r|\n)\Z/);
end

def authors_check(file, dirname)
    out = ""
    $nbFailAuthors += 1
    if File.basename(file) != "AUTHORS"
        out = "FAIL Filename\n"
    end
    if !authors_checkContent(File.read(file))
        out += "FAIL Content\n"
        print out.red
    elsif out == ""
        out = "OK\n"
        $nbFailAuthors -= 1
        print out.green
    else
        print out.red
    end
    noteFile = File.open("#{dirname}/NOTE", "w+");
    noteFile.write(out);
    noteFile.close();
end
