Moulinette C#
=============

## Presentation

This is my automatic correction system.  
All script were written in Ruby (2.0).

## Require

- Mac OS X, Linux or other UNIX like system (not Windows)
- make
- ruby (2.0 and best)
- mono (for executing C# code)
- student depot must be on a zip file and have an AUTHORS file like "\* cormer\_a\\n"

## How to use

### How to configure

Edit the file test\_acdc/source/correction.cmd with these following tags:

At the beginning of the file:

* to configure the tool
```
###CONFIG
```

* Set default timeout
```
\timeout
[0-9]+
\end
```

* Set list of source code file extension
```
\extension
[...]
\end
```

* Set the list of possible main function prototype
```
\mainPrototype
[...]
\end
```

* Replace the main function prototype by another
```
\mainReplacePrototype
[...]
\end
```

* end of config part
```
\cut
```

* to check if the repository compile
```
###COMPILE
```

* list of dirname to check the compile
```
\dirsouce
[...]
\end
```

* end of compile check
```
\cut
```

And after:

* number of the test (ex: 2.0.1)
```
\num
[...]
\end
```
* Your custom main function
```
\main
[...]
\end
```

* Edit source code
```
\edit
#\prototype
REGEX EXP
#\end

#\replace
STRING
#\end
\end
```

* Custom a function
```
\customfun
#\prototype
STRING
#\end

#\replace
STRING
#\end

#\custom
STRING
#\end
\end
```

* Input stream
```
\stdin
[...]
\end
```

* Output stream (regex)
```
\stdout
[...]
\end
```

* Output error stream (regex)
```
\stderr
[...]
\end
```

* For adding custom function on the source code
```
\fun
[...]
\end
```

* To use a custom run.sh file (use for execution)
```
\shfile
[...]
\end
```

* List of forbidden function
```
\forbidden
[...]
\end
```

* List of directory name possible of parent source code folder (ex: "ex0" for adding "ex0/*.cs")
```
\dirsource
[...]
\end
```

* Set a custom timeout for the test
```
\timeout
[0-9]+
\end
```

* Option when you just want to print stdout and stderr (no compare)
```
\justprint
```

* to finish a check config
```
\cut
```

All others lines are ignored.

### Exemple of configuration

This exemple work! All commentary line are ignored.

```
###COMPILE
```

This part will try to compile the repository without custom the main or check execution

In this exemple, I want to check first if all source code contain on the src (or Src if src is not found) compile.

```
\dirsource
src
Src
\end
```

After, I want to check if the source code contains on the ex0 directory (or if not found, EX0 or Ex0) compile.

```
\dirsource
ex0
EX0
Ex0
\end
```

```
\cut
```

The number of the test

```
\num
1.0.0
\end
```

Your custom main function

```
\main
Console.WriteLine(printHello());
\end
```

Input stream content

```
\stdin
\end
```


Out stream that you want at the end of execution (regex)

```
\stdout
(.|\n)*Hello World!\n(.|\n)*
\end
```


Error stream that you want at the end of execution (regex)

```
\stderr
\end
```

List of directory name possible

```
\dirsource
src
Src
\end
```


Don't forget \\cut to finish

```
\cut
```

The complete exemple:

```
###CONFIG

\timeout
5
\end

\extension
.cs
\end

\mainPrototype
static void Main(string[] args)
static void Main()
\end

\mainReplacePrototype
static void old_Main(string[] args)
\end

###COMPILE

\dirsource
src
Src
\end

\dirsource
ex0
EX0
Ex0
\end

\cut

\num
1.0.0
\end

\main
Console.WriteLine(printHello());
\end

\stdin
\end

\stdout
(.|\n)*Hello World!\n(.|\n)*
\end

\stderr
\end

\dirsource
src
Src
\end

\cut



\num
2.0.0
\end

\main
Console.WriteLine(printHello());
\end

\stdin
\end

\stdout
(.|\n)*Hello World!\n(.|\n)*
\end

\stderr
\end

\dirsource
src
Src
\end

\edit
#\prototype
(?!(public))(private)? int a;
#\end

#\replace
public int a;
#\end
\end

\customfun
#\prototype
public void printHey()
#\end

#\replace
public void old_printHey()
#\end

#\custom
{ Console.WriteLine("Hey!"); }
#\end
\end

\cut
```

### How to run

After to have edit correction.cmd, make:

```
./moulinette.rb
```

To test a student repository (like "rendu-tp-cormer\_a.zip"):

```
cd ./rendu-tp-cormer_a
./run_test.rb
```

If you want to regenerate the test suit for only one repository (for example, after to have edit the source code):

```
cd ./rendu-tp-cormer_a
./re_gen.rb
```

## AUTHOR

Cormery Antoine (cormer\_a) aka "Chasseur"

antoine.cormery@epita.fr
